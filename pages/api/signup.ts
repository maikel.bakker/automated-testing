import { NextApiRequest, NextApiResponse } from 'next'
import {validationSchema} from "../../forms/signup/step1";

export default async (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    await validationSchema.validate(_req.body, { abortEarly: false })
    res.status(200).json(_req.body)
  } catch (error) {
    res.status(500).json({ statusCode: 500, message: error.message, errors: error.errors })
  }
}
