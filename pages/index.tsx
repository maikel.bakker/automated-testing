import Link from 'next/link'
import Layout from '../components/Layout'
import {SignUpStep1} from "../forms/signup/step1";

const IndexPage = () => (
  <Layout title="Home | Next.js + TypeScript Example">
    <h1>Hello Next.js 👋</h1>
    <p>
      <Link href="/about">
        <a>About</a>
      </Link>
    </p>
    <SignUpStep1/>
  </Layout>
)

export default IndexPage
