import {fireEvent, render, waitFor} from "@testing-library/react";
import { Formik, Form } from 'formik'
import FormikTextInput, {TEST_IDS} from "./index";
import * as yup from 'yup'

const FORM_TEST_ID = 'form'

const setup = () => {
  const data = {
    errorMessage: 'Test is required',
    inputProps: {
      label: 'Test',
      name: 'test',
      type: 'email'
    }
  }
  const schema = yup.object().shape({
    test: yup.string().required(data.errorMessage)
  })
  const utils = render(
    <Formik
      initialValues={{ test: '' }}
      onSubmit={async (values) => values}
      validationSchema={schema}
    >
      <Form data-testid={FORM_TEST_ID}>
        <FormikTextInput {...data.inputProps} />
      </Form>
    </Formik>
  )

  const elements = {
    label: utils.getByTestId(TEST_IDS.LABEL),
    input: utils.getByTestId(TEST_IDS.INPUT),
    form: utils.getByTestId(FORM_TEST_ID)
  }

  return {
    data,
    ...elements,
    ...utils
  }
}

describe('Renders the correct elements', () => {
  it('Renders label', () => {
    const { label, data } = setup()
    expect(label).toHaveTextContent(data.inputProps.label)

  })

  it('Renders input', () => {
    const { input, data } = setup()
    expect(input).toHaveProperty('name', data.inputProps.name)
    expect(input).toHaveProperty('type', data.inputProps.type)
  })

  it('Renders error', async () => {
    const { form, getByTestId, data } = setup()

    fireEvent.submit(form)

    await waitFor( () => {
      expect(getByTestId(TEST_IDS.ERROR_MESSAGE)).toHaveTextContent(data.errorMessage)
    })
  })
})
