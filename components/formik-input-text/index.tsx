import React from "react";
import { useField } from "formik";

export const TEST_IDS = {
  LABEL: 'label',
  INPUT: 'input',
  ERROR_MESSAGE: 'error-message'
}

interface Props {
  label: string;
  name: string;
  type?: string;
}

export default function FormikTextInput (props: Props) {
  const { label, name, type = 'text', ...restProps } = props
  const [ field, meta ] = useField(name)

  return (
    <>
      <label data-testid={TEST_IDS.LABEL}>
        {label}
        <input data-testid={TEST_IDS.INPUT} type={type} {...field} {...restProps} />
      </label>
      {meta.touched && meta.error && (
        <div data-testid={TEST_IDS.ERROR_MESSAGE}>{meta.error}</div>
      )}
    </>
  );
}
