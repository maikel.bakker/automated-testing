import {fireEvent, render, waitFor} from "@testing-library/react";
import {api, SignUpStep1, TEST_IDS} from "./step1";

const mockPostSignUp = (api.postSignUp = jest.fn())
const formData = {
  firstName: 'Maikel',
  lastName: 'Bakker',
  email: 'maikel.bakker@touchtribe.nl'
}

const setup = () => {
  const utils = render(
    <SignUpStep1 />
  )
  const elements = {
    submit: utils.getByTestId(TEST_IDS.SUBMIT),
    firstNameInput: utils.getByLabelText('First Name'),
    lastNameInput: utils.getByLabelText('Last Name'),
    emailInput: utils.getByLabelText('Email')
  }
  return {
    ...elements,
    ...utils
  }
}

describe('Form is succesfully submitted', () => {
  beforeEach(() => {
    mockPostSignUp.mockResolvedValueOnce(formData)
  })

  it('Show success message', async () => {
    const {
      firstNameInput,
      lastNameInput,
      emailInput,
      submit,
      getByTestId
    } = setup()

    fireEvent.change(firstNameInput, { target: { value: formData.firstName } })
    fireEvent.change(lastNameInput, { target: { value: formData.lastName } })
    fireEvent.change(emailInput, { target: { value: formData.email } })

    fireEvent.click(submit)

    await waitFor(() => {
      const successMessage = getByTestId(TEST_IDS.SUCCESS_MESSAGE)
      expect(successMessage).toBeTruthy()
    })
  })
})
