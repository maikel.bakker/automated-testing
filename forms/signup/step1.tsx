import React, {useState} from 'react'
import * as Yup from 'yup'
import {
  Formik,
  Form
} from 'formik';
import FormikTextInput from "../../components/formik-input-text";
import axios from "axios";

export const TEST_IDS = {
  SUBMIT: 'submit',
  SUCCESS_MESSAGE: 'success-message'
}

export const validationSchema = Yup.object({
  firstName: Yup.string().required('First Name is required'),
  lastName: Yup.string().required('Last Name is required'),
  email: Yup.string().email('Please enter a valid email').required('Email is required')
})

interface FormValues {
  firstName: string,
  lastName: string,
  email: string
}

const initialValues: FormValues = {
  firstName: '',
  lastName: '',
  email: ''
}

export const api = {
  postSignUp: async (values: FormValues) => {
    return await axios.post('api/signup', values)
  }
}

export const SignUpStep1: React.FC<{}> = () => {
  const [ showSuccessMessage, setShowSuccessMessage ] = useState(false)
  return (
    <div>
      <h1>Enter your personal info</h1>
      <Formik
        initialValues={initialValues}
        onSubmit={async (values, actions) => {
          await api.postSignUp(values)
          setShowSuccessMessage(true)
          actions.setSubmitting(false)
          actions.resetForm()
        }}
        validationSchema={validationSchema}
      >
        <Form>
          <div>
            <FormikTextInput name="firstName" label="First Name" />
          </div>
          <div>
            <FormikTextInput name="lastName" label="Last Name" />
          </div>
          <div>
            <FormikTextInput name="email" label="Email" type="email" />
          </div>
          <button data-testid={TEST_IDS.SUBMIT} type="submit">Submit</button>
        </Form>
      </Formik>
      {showSuccessMessage && (
        <div data-testid={TEST_IDS.SUCCESS_MESSAGE}>Form successfully submitted</div>
      )}
    </div>
  );
};
